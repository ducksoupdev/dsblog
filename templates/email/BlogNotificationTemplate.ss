<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>$Subject</title>
</head>
<body style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6;-webkit-font-smoothing: antialiased;-webkit-text-size-adjust: none;width: 100% !important;height: 100%;background-color: #f6f6f6">

<!-- body -->
<table class="body-wrap" style="margin: 0;padding: 20px;font-family: sans-serif;font-size: 100%;line-height: 1.6;width: 100%;background-color: #f6f6f6"><tr style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"><td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"></td>
    <td class="preheader-container" style="margin: 0 auto !important;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6;display: block !important;max-width: 600px !important;clear: both !important">
        <div class="preheader-content" style="margin: 0;padding: 0;font-family: sans-serif;font-size: 0.8em;line-height: 1.6;padding-left: 20px;padding-right: 20px;color: #666">
            <table style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"><tr style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"><td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6">
                $Preheader
            </td>
            </tr></table></div>
    </td>
    <td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"></td>
</tr><tr style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"><td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"></td>
    <td class="container" bgcolor="#FFFFFF" style="margin: 0 auto !important;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6;border: 1px solid #f0f0f0;display: block !important;max-width: 600px !important;clear: both !important">

        <!-- content -->
        <div class="content" style="margin: 0 auto;padding: 20px;font-family: sans-serif;font-size: 100%;line-height: 1.6;max-width: 600px;display: block">
            <table style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6;width: 100%"><tr style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"><td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6">
                $Body
            </td>
            </tr></table></div>
        <!-- /content -->

    </td>
    <td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"></td>
</tr></table><!-- /body --><!-- footer --><table class="footer-wrap" style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6;width: 100%;clear: both !important"><tr style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"><td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"></td>
    <td class="container" style="margin: 0 auto !important;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6;display: block !important;max-width: 600px !important;clear: both !important">

        <!-- content -->
        <div class="content" style="margin: 0 auto;padding: 20px;font-family: sans-serif;font-size: 100%;line-height: 1.6;max-width: 600px;display: block">
            <table style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6;width: 100%"><tr style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"><td align="center" style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6">
            </td>
            </tr></table></div><!-- /content -->

    </td>
    <td style="margin: 0;padding: 0;font-family: sans-serif;font-size: 100%;line-height: 1.6"></td>
</tr></table><!-- /footer --></body>
</html>
