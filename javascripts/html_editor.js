/**
 * Created with JetBrains PhpStorm.
 * User: mlevy
 * Date: 03/05/13
 * Time: 13:02
 * To change this template use File | Settings | File Templates.
 */
$(function () {
    tinyMCE.init({
        theme : "advanced",
        mode: "specific_textareas",
        editor_deselector : "mceNoEditor",
        theme_advanced_toolbar_location : "top",
        theme_advanced_buttons1 : "bold,italic,underline,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,outdent,indent,separator,undo,redo,separator,link,unlink,image",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        height:"250px",
        width:"100%"
    });
});