# Blog module for SilverStripe v3.1.x

A simple module for creating and maintaining blog posts.

* Posts are created as DataObjects
* Posts are managed through ModelAdmin extension
* Posts can be tagged
* Tag and Date filters are provided in the templates
* Templates can be customised
* Post submission form for guest posts -

## Requirements

* Silverstripe CMS v3.1.x
* [TagField module](https://github.com/chillu/silverstripe-tagfield)
* [OptionalTreeDropdownField](https://github.com/richardsjoqvist/silverstripe-optionaltreedropdownfield)

## Installation

    composer require ducksoupdev/dsblog dev-master

After installation run /dev/build?flush=1

## Templates

The templates can be changed by creating a new set in the themes directory using your chosen theme. For example, if your chosen theme is 'simple', create a directory named 'simple_dsblog' and copy the templates directory from the module into that directory.

## TODO

* Blog comments

## Maintainer

Matt Levy - ducksoupdev (at) gmail (dot) com