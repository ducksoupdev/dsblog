<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mlevy
 * Date: 22/04/13
 * Time: 07:05
 * To change this template use File | Settings | File Templates.
 */
class BlogPostAdmin extends ModelAdmin {

    private static $url_segment = "blogposts";

    private static $menu_title = "Blog Posts";

    private static $menu_icon = "dsblog/images/icons/blogpost.png";

    private static $managed_models = array(
        "BlogPost" => array("title" => "Blog Posts")
    );

}