<?php
/**
 * Created by PhpStorm.
 * User: levym
 * Date: 26/02/14
 * Time: 08:26
 */

class InlineMailer extends Mailer {

    public function sendHTML($to, $from, $subject, $htmlContent, $attachedFiles = false, $customheaders = false,
                             $plainContent = false) {
        if ($customheaders && is_array($customheaders) == false) {
            echo "htmlEmail($to, $from, $subject, ...) could not send mail: improper \$customheaders passed:<BR>";
            dieprintr($customheaders);
        }

        $bodyIsUnicode = (strpos($htmlContent,"&#") !== false);
        $plainEncoding = "";

        // We generate plaintext content by default, but you can pass custom stuff
        $plainEncoding = '';
        if(!$plainContent) {
            $plainContent = Convert::xml2raw($htmlContent);
            if(isset($bodyIsUnicode) && $bodyIsUnicode) $plainEncoding = "base64";
        }

        // If the subject line contains extended characters, we must encode the
        $subject = Convert::xml2raw($subject);
        $subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";

        // Make the plain text part
        $headers["Content-Type"] = "text/plain; charset=utf-8";
        $headers["Content-Transfer-Encoding"] = $plainEncoding ? $plainEncoding : "quoted-printable";

        $plainPart = $this->processHeaders($headers, ($plainEncoding == "base64")
            ? chunk_split(base64_encode($plainContent),60)
            : wordwrap($this->QuotedPrintable_encode($plainContent),75));

        // Make the HTML part
        $headers["Content-Type"] = "text/html; charset=utf-8";

        // Add basic wrapper tags if the body tag hasn't been given
        if(stripos($htmlContent, '<body') === false) {
            $htmlContent =
                "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\">\n" .
                "<HTML><HEAD>\n" .
                "<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n" .
                "<STYLE type=\"text/css\"></STYLE>\n\n".
                "</HEAD>\n" .
                "<BODY bgColor=\"#ffffff\">\n" .
                $htmlContent .
                "\n</BODY>\n" .
                "</HTML>";
        }

        // make styles inline - Added by Matt Levy 17/02/2014
        $htmlDoc = new InlineStyle\InlineStyle($htmlContent);
        $htmlDoc->applyStylesheet($htmlDoc->extractStylesheets());
        $htmlContent = $htmlDoc->getHTML();
        SS_Log::log("HTML to send: " . $htmlContent, SS_Log::NOTICE);

        $headers["Content-Transfer-Encoding"] = "quoted-printable";
        $htmlPart = $this->processHeaders($headers, wordwrap($this->QuotedPrintable_encode($htmlContent),75));

        list($messageBody, $messageHeaders) = $this->encodeMultipart(
            array($plainPart,$htmlPart),
            "multipart/alternative"
        );

        // Messages with attachments are handled differently
        if($attachedFiles && is_array($attachedFiles)) {

            // The first part is the message itself
            $fullMessage = $this->processHeaders($messageHeaders, $messageBody);
            $messageParts = array($fullMessage);

            // Include any specified attachments as additional parts
            foreach($attachedFiles as $file) {
                if(isset($file['tmp_name']) && isset($file['name'])) {
                    $messageParts[] = $this->encodeFileForEmail($file['tmp_name'], $file['name']);
                } else {
                    $messageParts[] = $this->encodeFileForEmail($file);
                }
            }

            // We further wrap all of this into another multipart block
            list($fullBody, $headers) = $this->encodeMultipart($messageParts, "multipart/mixed");

            // Messages without attachments do not require such treatment
        } else {
            $headers = $messageHeaders;
            $fullBody = $messageBody;
        }

        // Email headers
        $headers["From"] = $this->validEmailAddr($from);

        // Messages with the X-SilverStripeMessageID header can be tracked
        if(isset($customheaders["X-SilverStripeMessageID"]) && defined('BOUNCE_EMAIL')) {
            $bounceAddress = BOUNCE_EMAIL;
        } else {
            $bounceAddress = $from;
        }

        // Strip the human name from the bounce address
        if(preg_match('/^([^<>]*)<([^<>]+)> *$/', $bounceAddress, $parts)) $bounceAddress = $parts[2];

        // $headers["Sender"] 		= $from;
        $headers["X-Mailer"]	= X_MAILER;
        if (!isset($customheaders["X-Priority"])) $headers["X-Priority"]	= 3;

        $headers = array_merge((array)$headers, (array)$customheaders);

        // the carbon copy header has to be 'Cc', not 'CC' or 'cc' -- ensure this.
        if (isset($headers['CC'])) { $headers['Cc'] = $headers['CC']; unset($headers['CC']); }
        if (isset($headers['cc'])) { $headers['Cc'] = $headers['cc']; unset($headers['cc']); }

        // the carbon copy header has to be 'Bcc', not 'BCC' or 'bcc' -- ensure this.
        if (isset($headers['BCC'])) {$headers['Bcc']=$headers['BCC']; unset($headers['BCC']); }
        if (isset($headers['bcc'])) {$headers['Bcc']=$headers['bcc']; unset($headers['bcc']); }

        // Send the email
        $headers = $this->processHeaders($headers);
        $to = $this->validEmailAddr($to);

        // Try it without the -f option if it fails
        if(!($result = @mail($to, $subject, $fullBody, $headers, escapeshellarg("-f$bounceAddress")))) {
            $result = mail($to, $subject, $fullBody, $headers);
        }

        return $result;
    }

} 