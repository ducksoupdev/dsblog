<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mlevy
 * Date: 02/05/13
 * Time: 22:07
 * To change this template use File | Settings | File Templates.
 */

class BlogPost extends DataObject {

    private static $singular_name = "Blog Post";

    private static $plural_name = "Blog Posts";

    private static $default_sort = "Created DESC";

    private static $db = array(
        "Title" => "Varchar(255)",
        "AuthorName" => "Varchar(255)",
        "AuthorEmail" => "Varchar(255)",
        "PublishDate" => "Date",
        "Summary" => "Text",
        "Description" => "HTMLText",
        "Approved" => "Boolean",
        "ApprovedOn" => "SS_Datetime",
        "URLSegment" => "Varchar(255)"
    );

    private static $has_one = array(
        "BlogPage" => "Page",
        "Image" => "Image"
    );

    private static $many_many = array(
        "Tags" => "BlogTag"
    );

    private static $summary_fields = array(
        "Title" => "Title",
        "PublishDate" => "Publish Date",
        "AuthorName" => "Author Name",
        "ApprovedOn" => "Approved On"
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->removeByName("ApprovedOn");
        $fields->removeByName("URLSegment");

        // add new tab named Image
//        $uploadField = new UploadField("Image", "Image");
//        $uploadField->getValidator()->setAllowedExtensions(array("jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "gif", "GIF"));
//        $fields->addFieldToTab("Root.Image", $uploadField);

        $bp = new OptionalTreeDropdownField("BlogPageID", "Blog Page", "Page");
        //$fields->replaceField("BlogPageID", $bp);
        $fields->removeByName("BlogPageID");
        $fields->insertAfter($bp, "Title");

        $sd = new DateField("PublishDate", "Start Date (dd/MM/yyyy)");
        $sd->setConfig("showcalendar", true);
        $sd->setConfig("showdropdown", true);
        $sd->setConfig("dateformat", "dd/MM/YYYY");
        $fields->replaceField("PublishDate", $sd);

        $tagField = new TagField("Tags", "Tags (space separated)", null, "BlogPost");
        $fields->insertAfter($tagField, "Summary");

        return $fields;
    }

    public function onBeforeWrite() {
        parent::onBeforeWrite();
        if ($this->Approved && !$this->ApprovedOn) {
            $this->ApprovedOn = date("Y-m-d H:i:s", time());
        }
        if ($this->Title) {
            $this->URLSegment = $this->generateURLSegment($this->Title);
        }
    }

    public function validate() {
        $result = parent::validate();
        if (!$this->Title || $this->Title == "") {
            $result->error("Title is a required field");
            return $result;
        } else if (!$this->BlogPageID) {
            $result->error("Please select a blog page");
            return $result;
        }
        return $result;
    }

    // return the link to view this blog post
    public function Link() {
        if ($blogPage = $this->BlogPage()) {
            $action = $blogPage->URLSegment . "/show/" . $this->ID . "-" . $this->URLSegment;
            //SS_Log::log($action, SS_Log::NOTICE);
            return $action;
        }
    }

    /* Copied from SiteTree */
    public function generateURLSegment($title) {
		$filter = URLSegmentFilter::create();
		$t = $filter->filter($title);

		// Fallback to generic page name if path is empty (= no valid, convertable characters)
		if(!$t || $t == "-" || $t == "-1") $t = "page-{$this->ID}";

		return $t;
	}

}