<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mlevy
 * Date: 02/05/13
 * Time: 22:15
 * To change this template use File | Settings | File Templates.
 */

class BlogPage extends Page {

    private static $db = array(
        "NumberToDisplayPerPage" => "Int",
        "EnablePostSubmission" => "Boolean",
        "Summary" => "HTMLVarchar"
    );

    private static $singular_name = "Blog Page";

    private static $plural_name = "Blog Pages";

    private static $description = "A page to display blog posts";

    private static $icon = "dsblog/images/icons/blogpage.png";

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // add to main content tab
        $fields->addFieldToTab('Root.Main', new TextField('Summary', 'Summary (Optional)'), 'Content');
        $fields->renameField("Content", "About Blog");

        $fields->addFieldToTab("Root.Blog", new DropdownField("NumberToDisplayPerPage", "Number to display per page", array(5 => 5, 10 => 10, 15 => 15, 20 => 20, 30 => 30, 40 => 40, 50 => 50), 5));

        $enablePostSubmissionField = new FieldGroup(new CheckboxField("EnablePostSubmission", "Enable blog post submissions"));
        $enablePostSubmissionField->setTitle("Submission");
        $fields->addFieldToTab("Root.Blog", $enablePostSubmissionField);

        return $fields;
    }

}

class BlogPage_Controller extends Page_Controller {

    //Allow our 'show' function as a URL action
    private static $allowed_actions = array(
        "show",
        "post",
        "about"
    );

    // Get the current blog post ID from the URL, if any
    // this can be new style URLs or old style ID urls
    public function getBlogPost() {
        $params = $this->getURLParams();
        if (preg_match("/^[0-9]+-/", $params["ID"])) {
            $idParts = preg_split("/-/", $params["ID"]);
            if (is_numeric($idParts[0]) && $blogPost = DataObject::get_by_id("BlogPost", (int)$idParts[0])) {
                return $blogPost;
            } else {
                return null;
            }
        } else if (is_numeric($params['ID']) && $blogPost = DataObject::get_by_id('BlogPost', (int)$params['ID'])) {
            return $blogPost;
        } else {
            return null;
        }
    }

    // Displays the blogpost detail page, using BlogPage_show.ss template
    public function show() {
        if ($blogPost = $this->getBlogPost()) {
            $Data = array(
                'BlogPost' => $blogPost
            );
            //return our $Data array to use on the page
            return $this->Customise($Data);
        } else {
            //blog post not found
            return $this->httpError(404, 'Sorry, that blog post could not be found');
        }
    }

    public function BlogPosts() {
        $limit = 5;
        $limit = intval($this->NumberToDisplayPerPage);
        $params = $this->getRequest()->getVars();

        $start = 0;
        if (isset($params["start"]) && $params["start"] != "") {
            $start = intval($params["start"]);
        }

        if (isset($params["month"]) && $params["month"] != "") {
            $id = $this->ID;
            $start = strtotime($params["month"] . "-01");
            $end = strtotime($params["month"] . "-31");

            // return posts for a particular month
            return BlogPost::get()
                ->sort("PublishDate DESC")
                ->filterByCallback(function($post) use ($id, $start, $end) {
                    if ($post->BlogPageID == $id && $post->Approved) {
                        // check the dates
                        $pdt = strtotime($post->PublishDate);
                        if ($pdt >= $start && $pdt <= $end) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                });
        } else {
            return BlogPost::get()
                ->filter(array("BlogPageID" => $this->ID, "Approved" => true))
                ->sort("PublishDate DESC")
                ->limit($limit, $start);
        }
    }

    public function Pagination() {
        $params = $this->getRequest()->getVars();
        if (isset($params["month"]) && $params["month"] != "") {
            return null;
        } else {
            $limit = 5;
            $limit = intval($this->NumberToDisplayPerPage);

            // get the total and create the pagination object
            $total = DB::query("SELECT COUNT(*) FROM BlogPost")->value();
            SS_Log::log("Total blog items: " . $total, SS_Log::NOTICE);

            if ($total <= $limit) {
                return null;
            }

            $pagination = new PaginatedList(new ArrayList(), $this->request);
            $pagination->setTotalItems($total);
            $pagination->setPageLength($limit);
            return $pagination;
        }
    }

    // Return our custom breadcrumbs - Bootstrap or others
    public function Breadcrumbs() {
        // Get the default breadcrumbs
        $Breadcrumbs = parent::Breadcrumbs();
        $title = null;

        if ($blogPost = $this->getBlogPost()) {
            $title = $blogPost->Title;
        } else if ($this->getAction() == "post" || $this->getAction() == "BlogPostForm") {
            $title = "Submit a blog post";
        }

        if (!is_null($title)) {
            $delimiter = "\n";

            // Explode them into their individual parts
            $Parts = explode($delimiter, $Breadcrumbs);

            // Count the parts
            $NumOfParts = count($Parts);

            // Change the last item to a link instead of just text
            $Parts[$NumOfParts-1] = ('<li><a href="' . $this->Link() . '">' . $this->Title . '</a></li>');

            // Add our extra piece on the end
            $Parts[] = "<li class=\"active\">" . $title . "</li>";

            // Return the imploded array
            $Breadcrumbs = implode($delimiter, $Parts);
        }

        return $Breadcrumbs;
    }

    public function post(SS_HTTPRequest $request) {
        if (!$this->EnablePostSubmission) {
            $this->httpError(404, "The requested page cannot be found");
        } else {
            return $this->renderWith(array("BlogPage_post", "Page"));
        }
    }

    public function BlogPostForm() {
        $fields = new FieldList(
            HiddenField::create("BlogPageID")->setValue($this->ID),
            TextField::create("Title", "Title")->setAttribute('required', true),
            TextField::create("AuthorName", "Your name")->setAttribute('required', true),
            EmailField::create("AuthorEmail", "Your email address")->setAttribute('required', true),
            $summaryField = TextareaField::create("Summary", "Short summary")->setAttribute('required', true),
            new HtmlEditorField("Description", "Description")
        );
        $summaryField->addExtraClass("mceNoEditor");
        $actions = new FieldList(
            FormAction::create("doBlogPostForm")->setTitle("Submit")
        );
        $validation = new RequiredFields(array("Title", "AuthorName", "AuthorEmail", "Summary"));
        $form = new Form($this, 'BlogPostForm', $fields, $actions, $validation);
        // Load the form with previously sent data
        $form->loadDataFrom($this->request->postVars());
        return $form;
    }

    public function doBlogPostForm($data, Form $form) {
        // create the blog post
        $blogPost = new BlogPost();
        $blogPost->BlogPageID = $data["BlogPageID"];
        $blogPost->Title = $data["Title"];
        $blogPost->AuthorName = $data["AuthorName"];
        $blogPost->AuthorEmail = $data["AuthorEmail"];
        $blogPost->PublishDate = date("Y-m-d H:i:s", time());
        $blogPost->Summary = $data["Summary"];
        $blogPost->Description = $data["Description"];
        $blogPost->Approved = false;
        $blogPost->write();

        // send email notification to admin
        $group = Group::get()->filter(array("Code" => "administrators"))->first();
        $members = $group->Members();
        if ($members->count() > 0) {
            // send an email
            $from = $data["AuthorName"] . " <" . $data["AuthorEmail"] . ">";
            $toe = array();
            foreach ($members as $member) {
                $toe[] = $member->getName() . " <" . $member->Email . ">";
            }
            $to = implode(", ", $toe);
            $subject = "New blog post submitted by " . $blogPost->AuthorName . " - " . $blogPost->Title;
            $email = new Email($from, $to, $subject, "");
            $email::set_mailer(new InlineMailer());
            $email->setTemplate("BlogNotificationTemplate");
            $message = "<p class=\"h3\">" . $blogPost->Title . "</p>";
            $message .= $blogPost->Description;
            $message .= "<hr size=\"1\" noshade=\"\" />";
            $message .= "<p>To approve the blog post, login to the CMS at:<br><a href=\"http://" . $_SERVER["SERVER_NAME"] . "/admin\">" . $_SERVER["SERVER_NAME"] . "/admin</a></p>";
            $email->populateTemplate(array(
                "Preheader" => $blogPost->Title,
                "Subject" => "New blog post submitted by " . $blogPost->AuthorName,
                "Body" => $message
            ));
            $email->send();
        }

        // show thanks for posting message
        return $this->customise(array(
            "FormData" => $data
        ))->renderWith(array("BlogPage_post_thanks", "Page"));
    }

    /* override the submenu exists function */
    public function submenuExists() {
		return true;
	}

    /* override the submenu function */
    public function Menu($level) {
        if ($level < 2) {
            return parent::Menu($level);
        } else {
            $monthNames = array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $posts = BlogPost::get()->filter(array("BlogPageID" => $this->ID, "Approved" => true))->sort("PublishDate DESC");
            $months = array();
            foreach ($posts as $po) {
                $pd = $po->PublishDate;
                $monthTag = substr($pd, 0, 7);
                if (!array_key_exists($monthTag, $months)) {
                    $mp = preg_split("/-/", $pd);
                    $formattedMonth = $monthNames[intval($mp[1])];
                    $months[$monthTag] = new ArrayData(array(
                        "Link" => $this->Link() . "?month=" . $monthTag,
                        "MenuTitle" => $formattedMonth . " " . $mp[0],
                        "Title" => $formattedMonth . " " . $mp[0],
                        "LinkingMode" => ""
                    ));
                }
            }
            return new ArrayList(array_values($months));
        }
    }

    public function about(SS_HTTPRequest $request) {
        $Data = array(
            'Content' => $this->Content
        );
        return $this->Customise($Data);
    }

    public function Dates() {
        return $this->Menu(2);
    }

    public function Tags() {
        return new ArrayData(array(
            "Link" => $this->Link() . "?tag=",
            "MenuTitle" => "Test tag",
            "Title" => "Test tag",
            "LinkingMode" => ""
        ));
    }

}