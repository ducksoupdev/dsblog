<?php
/**
 * Created by PhpStorm.
 * User: levym
 * Date: 27/01/14
 * Time: 08:17
 */

class BlogTag extends DataObject {

    private static $db = array(
        "Title" => "Varchar(255)"
    );

    private static $belongs_many_many = array(
        "Posts" => "BlogPost"
    );

    public function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->replaceField("Title", new ReadonlyField("Title"));
        return $fields;
    }

} 